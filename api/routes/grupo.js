const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.grupo;
  app
    .route("/grupo")
    .get(verifyToken, controller.getGrupo)
    .post(verifyToken, controller.createGrupo)
    .put(verifyToken, controller.putGrupo)
    .delete(verifyToken, controller.deleteGrupo);
};
