const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.sistema;
  app
    .route("/sistema")
    .get(verifyToken, controller.getSistema)
    .post(verifyToken, controller.createSistema)
    .put(verifyToken, controller.putSistema)
    .delete(verifyToken, controller.deleteSistema);
};