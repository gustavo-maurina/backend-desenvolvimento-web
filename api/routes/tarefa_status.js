const { verifyToken } = require("../../config/auth");

module.exports = (app) => {
  const controller = app.controllers.tarefa_status;
  app
    .route("/tarefa_status")
    .get(verifyToken, controller.getTarefa_status)
    .post(verifyToken, controller.createTarefa_status)
    .put(verifyToken, controller.putTarefa_status)
    .delete(verifyToken, controller.deleteTarefa_status);
};