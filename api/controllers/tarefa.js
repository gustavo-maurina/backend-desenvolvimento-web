const { restart } = require("nodemon");

module.exports = (app) => {
	const controller = {};
	controller.createTarefa = function (req, res, next) {
		const titulo = req.body.titulo;
		const descricao = req.body.descricao || "";
		const id_projeto = req.body.id_projeto || "";
		const id_criador = req.body.id_criador || "";
		const id_dev = req.body.id_dev || "";
		const tempo_estimado = req.body.tempo_estimado || 0;
		const data_inicio = req.body.data_inicio || "2021-06-22 00:00:00";
		const data_fim = req.body.data_fim || "2021-06-22 00:00:00";
		const id_pai_tarefa = req.body.id_pai_tarefa || 1;
		const id_tipo_tarefa = req.body.id_tipo_tarefa || 1;
		const id_status_tarefa = req.body.id_status_tarefa || "";
		const data_inicio_dev = req.body.data_inicio_dev || "2021-06-22 00:00:00";
		const data_fim_dev = req.body.data_fim_dev || "2021-06-22 00:00:00";
		const tempo_realizado = req.body.tempo_realizado || 0;
		const authorized = req.body.authorized || true;
		const id_prioridade = req.body.id_prioridade || "";
		const complexidade = req.body.complexidade || "";
		const impacto = req.body.impacto || "";
		const id_grupo = req.body.id_grupo || "";

		app.db
			.none(
				`INSERT INTO public.tarefa (titulo, descricao, id_projeto, id_criador, id_dev, tempo_estimado, data_inicio, data_fim, id_pai_tarefa, id_tipo_tarefa, id_status_tarefa, data_inicio_dev, data_fim_dev, created_at, tempo_realizado, authorized, id_prioridade, complexidade, impacto, id_grupo) VALUES ('${titulo}', '${descricao}', ${id_projeto}, ${id_criador}, ${id_dev}, ${tempo_estimado}, '${data_inicio}', '${data_fim}', ${id_pai_tarefa}, ${id_tipo_tarefa}, ${id_status_tarefa}, '${data_inicio_dev}', '${data_fim_dev}', CURRENT_TIMESTAMP, ${tempo_realizado}, ${authorized}, ${id_prioridade}, ${complexidade}, ${impacto}, ${id_grupo});`
			)
			.then((data) => {
				res.status(200).json("Tarefa criada com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.putTarefa = function (req, res, next) {
		const id = req.query.id;
		const titulo = req.body.titulo;
		const descricao = req.body.descricao || "";
		const id_projeto = req.body.id_projeto || "";
		const id_criador = req.body.id_criador || "";
		const id_dev = req.body.id_dev || "";
		const tempo_estimado = req.body.tempo_estimado || 0;
		const data_inicio = req.body.data_inicio || "2021-06-22 00:00:00";
		const data_fim = req.body.data_fim || "2021-06-22 00:00:00";
		const id_pai_tarefa = req.body.id_pai_tarefa || 1;
		const id_tipo_tarefa = req.body.id_tipo_tarefa || 1;
		const id_status_tarefa = req.body.id_status_tarefa || "";
		const data_inicio_dev = req.body.data_inicio_dev || "2021-06-22 00:00:00";
		const data_fim_dev = req.body.data_fim_dev || "2021-06-22 00:00:00";
		const tempo_realizado = req.body.tempo_realizado || 0;
		const authorized = req.body.authorized || true;
		const id_prioridade = req.body.id_prioridade || "";
		const complexidade = req.body.complexidade || "";
		const impacto = req.body.impacto || "";
		const id_grupo = req.body.id_grupo || "";

		app.db
			.none(
				`UPDATE public.tarefa SET titulo = '${titulo}', descricao = '${descricao}', id_projeto = ${id_projeto}, id_criador = ${id_criador}, id_dev = ${id_dev}, tempo_estimado = ${tempo_estimado}, data_inicio = '${data_inicio}', data_fim = '${data_fim}', id_pai_tarefa = ${id_pai_tarefa}, id_tipo_tarefa = ${id_tipo_tarefa}, id_status_tarefa = ${id_status_tarefa}, data_inicio_dev = '${data_inicio_dev}', data_fim_dev = '${data_fim_dev}', updated_at = CURRENT_TIMESTAMP, tempo_realizado = ${tempo_realizado}, authorized = ${authorized}, id_prioridade = ${id_prioridade}, complexidade = ${complexidade}, impacto = ${impacto}, id_grupo = '${id_grupo}' WHERE id = ${id}`
			)
			.then((data) => {
				res.status(200).json("Tarefa editada com sucesso");
			})
			.catch(function (err) {
				return next(err);
			});
	};

	controller.deleteTarefa = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(`DELETE from public.tarefa WHERE id = ${id}`)
				.then((data) => {
					res.status(200).json("Tarefa removida com sucesso.");
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			return res.status(500).json("Id necessário.");
		}
	};

	controller.getTarefa = function (req, res, next) {
		const id = req.query.id;

		if (id) {
			app.db
				.any(
					`select
					* from public.tarefa tar where tar.id = ${id};`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		} else {
			app.db
				.any(
					`select
						tar.id as id,
						tar.titulo as tarefa,
						tar.descricao as descricao,
						tar.tempo_estimado as duracao_estimada,
						to_char(tar.data_inicio, 'DD/MM/YYYY') as inicio,
						to_char(tar.data_fim, 'DD/MM/YYYY') as fim,
						to_char(tar.created_at, 'DD/MM/YYYY') as criado,
						to_char(tar.updated_at, 'DD/MM/YYYY') as atualizado,
						tar.tempo_realizado as duracao_real,
						tar.authorized as autorizado,
						tar.complexidade as complexidade,
						tar.impacto as impacto,
						pro.titulo as projeto,
						tar_tip.descricao as tarefa_tipo,
						tar_sta.descricao as tarefa_status,
						pri.descricao as prioridade,
						gru.descricao as grupo,
						usu.nome as usuario,
						usu2.nome as desenvolvedor
						from public.tarefa tar
							left join public.projeto pro
									on tar.id_projeto = pro.id
							left join public.tarefa_tipo tar_tip
									on tar.id_tipo_tarefa = tar_tip.id
							left join public.tarefa_status tar_sta
									on tar.id_status_tarefa = tar_sta.id
							left join public.prioridade pri
									on tar.id_prioridade = pri.id
							left join public.grupo gru
									on tar.id_grupo = gru.id
							left join public.usuario usu
									on tar.id_criador = usu.id_usuario
							left join public.usuario usu2
									on tar.id_dev = usu2.id_usuario;`
				)
				.then((data) => {
					res.status(200).json(data);
				})
				.catch(function (err) {
					return next(err);
				});
		}
	};

	return controller;
};
