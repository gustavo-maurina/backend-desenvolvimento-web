const jwt = require("jsonwebtoken");
const SECRET = "test_api";

module.exports = {
  verifyToken: function (req, res, next) {
    const token = req.headers["authorization"];
    jwt.verify(token, SECRET, (err, decoded) => {
      if (err) return res.status(401).end();
      req.userId = decoded.userId;
      next();
    });
  },
};
